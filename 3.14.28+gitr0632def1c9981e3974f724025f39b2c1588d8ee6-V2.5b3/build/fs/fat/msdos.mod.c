#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xf9f72e25, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x13c4061a, __VMLINUX_SYMBOL_STR(fat_detach) },
	{ 0x405c1144, __VMLINUX_SYMBOL_STR(get_seconds) },
	{ 0x2189d9f3, __VMLINUX_SYMBOL_STR(drop_nlink) },
	{ 0xc916a2b, __VMLINUX_SYMBOL_STR(mark_buffer_dirty_inode) },
	{ 0xfc6262fe, __VMLINUX_SYMBOL_STR(__mark_inode_dirty) },
	{ 0x349cba85, __VMLINUX_SYMBOL_STR(strchr) },
	{ 0xb12c7f9e, __VMLINUX_SYMBOL_STR(fat_flush_inodes) },
	{ 0x88797b41, __VMLINUX_SYMBOL_STR(inc_nlink) },
	{ 0x4836f3ec, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x8c7447c2, __VMLINUX_SYMBOL_STR(mount_bdev) },
	{ 0xe4d04b94, __VMLINUX_SYMBOL_STR(fat_sync_inode) },
	{ 0x7b77f649, __VMLINUX_SYMBOL_STR(fat_add_entries) },
	{ 0x248052df, __VMLINUX_SYMBOL_STR(fat_remove_entries) },
	{ 0xd32b45b5, __VMLINUX_SYMBOL_STR(fat_alloc_new_dir) },
	{ 0x99ecbcc0, __VMLINUX_SYMBOL_STR(fat_fill_super) },
	{ 0xd12de483, __VMLINUX_SYMBOL_STR(fat_build_inode) },
	{ 0x8de489b9, __VMLINUX_SYMBOL_STR(fat_attach) },
	{ 0x71c90087, __VMLINUX_SYMBOL_STR(memcmp) },
	{ 0x5448e0bc, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0xdbc1b659, __VMLINUX_SYMBOL_STR(set_nlink) },
	{ 0x49e5fade, __VMLINUX_SYMBOL_STR(sync_dirty_buffer) },
	{ 0xb73ce0e2, __VMLINUX_SYMBOL_STR(fat_getattr) },
	{ 0x4b3e3569, __VMLINUX_SYMBOL_STR(__brelse) },
	{ 0xcb3478c7, __VMLINUX_SYMBOL_STR(kill_block_super) },
	{ 0x6f20960a, __VMLINUX_SYMBOL_STR(full_name_hash) },
	{ 0xef420360, __VMLINUX_SYMBOL_STR(fat_scan) },
	{ 0x34ca0fa, __VMLINUX_SYMBOL_STR(register_filesystem) },
	{ 0x14ed6206, __VMLINUX_SYMBOL_STR(__fat_fs_error) },
	{ 0xef18783, __VMLINUX_SYMBOL_STR(d_splice_alias) },
	{ 0xc5253573, __VMLINUX_SYMBOL_STR(fat_setattr) },
	{ 0x52991a08, __VMLINUX_SYMBOL_STR(fat_free_clusters) },
	{ 0xfeb9b2de, __VMLINUX_SYMBOL_STR(fat_get_dotdot_entry) },
	{ 0x6a532721, __VMLINUX_SYMBOL_STR(unregister_filesystem) },
	{ 0x36be8af9, __VMLINUX_SYMBOL_STR(fat_time_unix2fat) },
	{ 0x7380ff1e, __VMLINUX_SYMBOL_STR(fat_dir_empty) },
	{ 0x626db439, __VMLINUX_SYMBOL_STR(d_instantiate) },
	{ 0x9458a53b, __VMLINUX_SYMBOL_STR(clear_nlink) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "8F7AADEA0FD8C97BB3543D8");
