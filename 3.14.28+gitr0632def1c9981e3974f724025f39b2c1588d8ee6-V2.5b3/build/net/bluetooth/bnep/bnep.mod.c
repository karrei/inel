#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xf9f72e25, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x2897cb89, __VMLINUX_SYMBOL_STR(sock_init_data) },
	{ 0xfbc74f64, __VMLINUX_SYMBOL_STR(__copy_from_user) },
	{ 0x22e1ae6f, __VMLINUX_SYMBOL_STR(up_read) },
	{ 0x7feb4dee, __VMLINUX_SYMBOL_STR(kernel_sendmsg) },
	{ 0xfd443a5c, __VMLINUX_SYMBOL_STR(sockfd_lookup) },
	{ 0x67c2fa54, __VMLINUX_SYMBOL_STR(__copy_to_user) },
	{ 0xf4e6f02f, __VMLINUX_SYMBOL_STR(sock_no_setsockopt) },
	{ 0x7f73b35c, __VMLINUX_SYMBOL_STR(sock_no_getsockopt) },
	{ 0x4c86184b, __VMLINUX_SYMBOL_STR(remove_wait_queue) },
	{ 0xb80449e, __VMLINUX_SYMBOL_STR(eth_change_mtu) },
	{ 0x2a3aa678, __VMLINUX_SYMBOL_STR(_test_and_clear_bit) },
	{ 0xa2b6b266, __VMLINUX_SYMBOL_STR(sock_no_getname) },
	{ 0x33ba5cd4, __VMLINUX_SYMBOL_STR(param_ops_bool) },
	{ 0x73a61d06, __VMLINUX_SYMBOL_STR(bt_sock_register) },
	{ 0x67f07456, __VMLINUX_SYMBOL_STR(kthread_create_on_node) },
	{ 0xb0ebcf38, __VMLINUX_SYMBOL_STR(sock_no_poll) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x455293f6, __VMLINUX_SYMBOL_STR(down_read) },
	{ 0xb61a0c3b, __VMLINUX_SYMBOL_STR(bt_err) },
	{ 0x7c640527, __VMLINUX_SYMBOL_STR(bt_info) },
	{ 0x2bb5a16f, __VMLINUX_SYMBOL_STR(__pskb_pull_tail) },
	{ 0xba55af10, __VMLINUX_SYMBOL_STR(sock_no_mmap) },
	{ 0xffd5a395, __VMLINUX_SYMBOL_STR(default_wake_function) },
	{ 0x49bf32b2, __VMLINUX_SYMBOL_STR(sock_no_recvmsg) },
	{ 0x52708884, __VMLINUX_SYMBOL_STR(bt_sock_unlink) },
	{ 0xfa2a45e, __VMLINUX_SYMBOL_STR(__memzero) },
	{ 0x5f754e5a, __VMLINUX_SYMBOL_STR(memset) },
	{ 0x243a9f84, __VMLINUX_SYMBOL_STR(netif_rx_ni) },
	{ 0xb6497789, __VMLINUX_SYMBOL_STR(sock_no_socketpair) },
	{ 0x7ea76bb5, __VMLINUX_SYMBOL_STR(sk_alloc) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xa054a9be, __VMLINUX_SYMBOL_STR(sock_no_bind) },
	{ 0x71c90087, __VMLINUX_SYMBOL_STR(memcmp) },
	{ 0xcc1fb551, __VMLINUX_SYMBOL_STR(baswap) },
	{ 0x557051e6, __VMLINUX_SYMBOL_STR(free_netdev) },
	{ 0xb0203b88, __VMLINUX_SYMBOL_STR(register_netdev) },
	{ 0x9dc1dca2, __VMLINUX_SYMBOL_STR(sock_no_listen) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xc7f32898, __VMLINUX_SYMBOL_STR(sock_no_accept) },
	{ 0xac7eada4, __VMLINUX_SYMBOL_STR(sk_free) },
	{ 0x8fea24bd, __VMLINUX_SYMBOL_STR(bt_sock_unregister) },
	{ 0x195c88e, __VMLINUX_SYMBOL_STR(skb_pull) },
	{ 0x83211609, __VMLINUX_SYMBOL_STR(up_write) },
	{ 0xe987ee72, __VMLINUX_SYMBOL_STR(init_net) },
	{ 0xa958ed4, __VMLINUX_SYMBOL_STR(down_write) },
	{ 0xbb18c840, __VMLINUX_SYMBOL_STR(fput) },
	{ 0x1e57f418, __VMLINUX_SYMBOL_STR(sock_no_shutdown) },
	{ 0x7fa279c2, __VMLINUX_SYMBOL_STR(bt_sock_link) },
	{ 0xd5e7f44e, __VMLINUX_SYMBOL_STR(module_put) },
	{ 0x888cde51, __VMLINUX_SYMBOL_STR(skb_queue_tail) },
	{ 0xc6cbbc89, __VMLINUX_SYMBOL_STR(capable) },
	{ 0x78feb030, __VMLINUX_SYMBOL_STR(proto_register) },
	{ 0xafc48bde, __VMLINUX_SYMBOL_STR(__alloc_skb) },
	{ 0x1000e51, __VMLINUX_SYMBOL_STR(schedule) },
	{ 0xe76c6a37, __VMLINUX_SYMBOL_STR(kfree_skb) },
	{ 0x24d60aa2, __VMLINUX_SYMBOL_STR(proto_unregister) },
	{ 0x2df140c3, __VMLINUX_SYMBOL_STR(alloc_netdev_mqs) },
	{ 0xd270cb10, __VMLINUX_SYMBOL_STR(eth_type_trans) },
	{ 0x3da970ff, __VMLINUX_SYMBOL_STR(__module_put_and_exit) },
	{ 0x37643a5d, __VMLINUX_SYMBOL_STR(wake_up_process) },
	{ 0x103e5c5b, __VMLINUX_SYMBOL_STR(ether_setup) },
	{ 0x50d7c89f, __VMLINUX_SYMBOL_STR(__module_get) },
	{ 0xd85cd67e, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0xcdc01fc7, __VMLINUX_SYMBOL_STR(sock_no_connect) },
	{ 0xc0056be5, __VMLINUX_SYMBOL_STR(_raw_write_unlock_bh) },
	{ 0x89c857e0, __VMLINUX_SYMBOL_STR(eth_validate_addr) },
	{ 0xc7bcbc8d, __VMLINUX_SYMBOL_STR(add_wait_queue) },
	{ 0x9d669763, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0xb7b61546, __VMLINUX_SYMBOL_STR(crc32_be) },
	{ 0x5ecafbd0, __VMLINUX_SYMBOL_STR(sock_no_sendmsg) },
	{ 0x5a18eb55, __VMLINUX_SYMBOL_STR(set_user_nice) },
	{ 0x6ec9ccdb, __VMLINUX_SYMBOL_STR(_raw_write_lock_bh) },
	{ 0xf6067b3a, __VMLINUX_SYMBOL_STR(bt_procfs_init) },
	{ 0xda42a104, __VMLINUX_SYMBOL_STR(skb_dequeue) },
	{ 0x8239f783, __VMLINUX_SYMBOL_STR(unregister_netdev) },
	{ 0x676bbc0f, __VMLINUX_SYMBOL_STR(_set_bit) },
	{ 0xb6526459, __VMLINUX_SYMBOL_STR(__netif_schedule) },
	{ 0x49ebacbd, __VMLINUX_SYMBOL_STR(_clear_bit) },
	{ 0x9751cc6c, __VMLINUX_SYMBOL_STR(bt_procfs_cleanup) },
	{ 0xe914e41e, __VMLINUX_SYMBOL_STR(strcpy) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=bluetooth";


MODULE_INFO(srcversion, "6A771F352A1D9FFC4071711");
