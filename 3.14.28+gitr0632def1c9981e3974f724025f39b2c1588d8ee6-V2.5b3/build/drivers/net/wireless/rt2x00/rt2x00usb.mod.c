#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xf9f72e25, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xa31d5a02, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x12da5bb2, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x6d70212, __VMLINUX_SYMBOL_STR(skb_pad) },
	{ 0xa54886e2, __VMLINUX_SYMBOL_STR(dev_set_drvdata) },
	{ 0x2a3aa678, __VMLINUX_SYMBOL_STR(_test_and_clear_bit) },
	{ 0xcd836611, __VMLINUX_SYMBOL_STR(usb_kill_urb) },
	{ 0xa6eeae34, __VMLINUX_SYMBOL_STR(rt2x00lib_resume) },
	{ 0x4836f3ec, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x5115b907, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x14cf1ca6, __VMLINUX_SYMBOL_STR(rt2x00lib_dmastart) },
	{ 0x3bd1c8a1, __VMLINUX_SYMBOL_STR(rt2x00queue_flush_queue) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x87d7c4dc, __VMLINUX_SYMBOL_STR(usb_control_msg) },
	{ 0x3bd47fe9, __VMLINUX_SYMBOL_STR(rt2x00queue_for_each_entry) },
	{ 0x5448e0bc, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0xee082ab1, __VMLINUX_SYMBOL_STR(rt2x00queue_start_queue) },
	{ 0x8e865d3c, __VMLINUX_SYMBOL_STR(arm_delay_ops) },
	{ 0x1746ab68, __VMLINUX_SYMBOL_STR(rt2x00lib_remove_dev) },
	{ 0x348892a8, __VMLINUX_SYMBOL_STR(usb_submit_urb) },
	{ 0x9346bbc4, __VMLINUX_SYMBOL_STR(kmem_cache_alloc) },
	{ 0xc4dce907, __VMLINUX_SYMBOL_STR(usb_get_dev) },
	{ 0x5a1340ed, __VMLINUX_SYMBOL_STR(rt2x00queue_get_entry) },
	{ 0x37674cd7, __VMLINUX_SYMBOL_STR(usb_reset_device) },
	{ 0x3bd1b1f6, __VMLINUX_SYMBOL_STR(msecs_to_jiffies) },
	{ 0x5d69e0f5, __VMLINUX_SYMBOL_STR(rt2x00lib_rxdone) },
	{ 0xa36cddd0, __VMLINUX_SYMBOL_STR(usb_put_dev) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x9d669763, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0xd7ebb984, __VMLINUX_SYMBOL_STR(ieee80211_alloc_hw) },
	{ 0x54d8b7ba, __VMLINUX_SYMBOL_STR(hrtimer_init) },
	{ 0x290d8440, __VMLINUX_SYMBOL_STR(ieee80211_free_hw) },
	{ 0x9522c3fe, __VMLINUX_SYMBOL_STR(dev_warn) },
	{ 0x3a8befb5, __VMLINUX_SYMBOL_STR(rt2x00lib_txdone_noinfo) },
	{ 0x676bbc0f, __VMLINUX_SYMBOL_STR(_set_bit) },
	{ 0xb2d48a2e, __VMLINUX_SYMBOL_STR(queue_work_on) },
	{ 0xca54fee, __VMLINUX_SYMBOL_STR(_test_and_set_bit) },
	{ 0x49ebacbd, __VMLINUX_SYMBOL_STR(_clear_bit) },
	{ 0x28473d67, __VMLINUX_SYMBOL_STR(rt2x00lib_dmadone) },
	{ 0xe744154e, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0xa6886f8b, __VMLINUX_SYMBOL_STR(usb_free_urb) },
	{ 0x36f4128e, __VMLINUX_SYMBOL_STR(rt2x00lib_probe_dev) },
	{ 0x57c86216, __VMLINUX_SYMBOL_STR(rt2x00queue_stop_queue) },
	{ 0x3d59838f, __VMLINUX_SYMBOL_STR(rt2x00lib_suspend) },
	{ 0xcc09a214, __VMLINUX_SYMBOL_STR(usb_alloc_urb) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=rt2x00lib,mac80211";


MODULE_INFO(srcversion, "1C7B3D09AA920FB776204BA");
