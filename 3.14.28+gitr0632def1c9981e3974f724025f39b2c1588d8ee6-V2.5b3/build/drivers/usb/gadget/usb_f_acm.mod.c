#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xf9f72e25, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xa31d5a02, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x499cd2a6, __VMLINUX_SYMBOL_STR(usb_gstrings_attach) },
	{ 0x918e91ae, __VMLINUX_SYMBOL_STR(usb_free_all_descriptors) },
	{ 0x89fcdbf7, __VMLINUX_SYMBOL_STR(gserial_connect) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0x3b772979, __VMLINUX_SYMBOL_STR(usb_function_unregister) },
	{ 0x140752ce, __VMLINUX_SYMBOL_STR(usb_put_function_instance) },
	{ 0x5115b907, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x3c9f40bc, __VMLINUX_SYMBOL_STR(usb_ep_autoconfig) },
	{ 0xa0e9f060, __VMLINUX_SYMBOL_STR(gserial_disconnect) },
	{ 0x2dae7baf, __VMLINUX_SYMBOL_STR(gs_alloc_req) },
	{ 0xc1f90134, __VMLINUX_SYMBOL_STR(config_group_init_type_name) },
	{ 0x33bfdca2, __VMLINUX_SYMBOL_STR(gserial_alloc_line) },
	{ 0x1a1612c, __VMLINUX_SYMBOL_STR(usb_function_register) },
	{ 0x9346bbc4, __VMLINUX_SYMBOL_STR(kmem_cache_alloc) },
	{ 0x463fcd24, __VMLINUX_SYMBOL_STR(config_ep_by_speed) },
	{ 0x9c0bd51f, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x9d669763, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0xb6652875, __VMLINUX_SYMBOL_STR(gserial_free_line) },
	{ 0xa81dddb5, __VMLINUX_SYMBOL_STR(usb_assign_descriptors) },
	{ 0x2e61e5e8, __VMLINUX_SYMBOL_STR(usb_interface_id) },
	{ 0xfe6e13df, __VMLINUX_SYMBOL_STR(gs_free_req) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=libcomposite,u_serial,configfs";


MODULE_INFO(srcversion, "10C7FC52BAA07AF57EA286B");
