#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xf9f72e25, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xa31d5a02, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x499cd2a6, __VMLINUX_SYMBOL_STR(usb_gstrings_attach) },
	{ 0x918e91ae, __VMLINUX_SYMBOL_STR(usb_free_all_descriptors) },
	{ 0x97255bdf, __VMLINUX_SYMBOL_STR(strlen) },
	{ 0xe70f877f, __VMLINUX_SYMBOL_STR(gether_get_qmult) },
	{ 0x87f8c8f8, __VMLINUX_SYMBOL_STR(gether_setup_name_default) },
	{ 0xc27bc839, __VMLINUX_SYMBOL_STR(netif_carrier_on) },
	{ 0x2a3aa678, __VMLINUX_SYMBOL_STR(_test_and_clear_bit) },
	{ 0xac5137e9, __VMLINUX_SYMBOL_STR(netif_carrier_off) },
	{ 0xef6f08d7, __VMLINUX_SYMBOL_STR(__dev_kfree_skb_any) },
	{ 0x4836f3ec, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x5661a1, __VMLINUX_SYMBOL_STR(gether_get_ifname) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0x4eb3011f, __VMLINUX_SYMBOL_STR(skb_realloc_headroom) },
	{ 0x8878cfa6, __VMLINUX_SYMBOL_STR(gether_cleanup) },
	{ 0x3b772979, __VMLINUX_SYMBOL_STR(usb_function_unregister) },
	{ 0x965cbc06, __VMLINUX_SYMBOL_STR(skb_trim) },
	{ 0x3ed4cfeb, __VMLINUX_SYMBOL_STR(gether_set_host_addr) },
	{ 0xf81f6b5a, __VMLINUX_SYMBOL_STR(gether_get_dev_addr) },
	{ 0xfa2a45e, __VMLINUX_SYMBOL_STR(__memzero) },
	{ 0xebd9ac0d, __VMLINUX_SYMBOL_STR(gether_connect) },
	{ 0x140752ce, __VMLINUX_SYMBOL_STR(usb_put_function_instance) },
	{ 0x5a5a94a6, __VMLINUX_SYMBOL_STR(kstrtou8) },
	{ 0x5115b907, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x34908c14, __VMLINUX_SYMBOL_STR(print_hex_dump_bytes) },
	{ 0x94a28e7d, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x3c9f40bc, __VMLINUX_SYMBOL_STR(usb_ep_autoconfig) },
	{ 0x557051e6, __VMLINUX_SYMBOL_STR(free_netdev) },
	{ 0x4c506785, __VMLINUX_SYMBOL_STR(gether_set_gadget) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xc2de76d3, __VMLINUX_SYMBOL_STR(skb_push) },
	{ 0x5448e0bc, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x9979fce0, __VMLINUX_SYMBOL_STR(gether_get_host_addr) },
	{ 0xc1f90134, __VMLINUX_SYMBOL_STR(config_group_init_type_name) },
	{ 0xe7ba371e, __VMLINUX_SYMBOL_STR(gether_set_qmult) },
	{ 0x124f01ca, __VMLINUX_SYMBOL_STR(gether_get_host_addr_u8) },
	{ 0x195c88e, __VMLINUX_SYMBOL_STR(skb_pull) },
	{ 0x1a1612c, __VMLINUX_SYMBOL_STR(usb_function_register) },
	{ 0x888cde51, __VMLINUX_SYMBOL_STR(skb_queue_tail) },
	{ 0x32b4cf6, __VMLINUX_SYMBOL_STR(gether_register_netdev) },
	{ 0x9346bbc4, __VMLINUX_SYMBOL_STR(kmem_cache_alloc) },
	{ 0xbd0c07d, __VMLINUX_SYMBOL_STR(gether_set_dev_addr) },
	{ 0x463fcd24, __VMLINUX_SYMBOL_STR(config_ep_by_speed) },
	{ 0xf4f95e91, __VMLINUX_SYMBOL_STR(gether_disconnect) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x9d669763, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0xa81dddb5, __VMLINUX_SYMBOL_STR(usb_assign_descriptors) },
	{ 0x2e61e5e8, __VMLINUX_SYMBOL_STR(usb_interface_id) },
	{ 0x676bbc0f, __VMLINUX_SYMBOL_STR(_set_bit) },
	{ 0xb6526459, __VMLINUX_SYMBOL_STR(__netif_schedule) },
	{ 0xf4287299, __VMLINUX_SYMBOL_STR(dev_get_stats) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=libcomposite,u_ether,configfs";


MODULE_INFO(srcversion, "E8397E2F4BDA4DE28D2F1D2");
