#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xf9f72e25, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x33ba5cd4, __VMLINUX_SYMBOL_STR(param_ops_bool) },
	{ 0xdd6fa2e7, __VMLINUX_SYMBOL_STR(usb_ep_autoconfig_reset) },
	{ 0x5d41c87c, __VMLINUX_SYMBOL_STR(param_ops_charp) },
	{ 0x140752ce, __VMLINUX_SYMBOL_STR(usb_put_function_instance) },
	{ 0x1a03e192, __VMLINUX_SYMBOL_STR(usb_composite_overwrite_options) },
	{ 0x6f8f7b1, __VMLINUX_SYMBOL_STR(_dev_info) },
	{ 0x64a6cc, __VMLINUX_SYMBOL_STR(usb_composite_probe) },
	{ 0xbec59e2a, __VMLINUX_SYMBOL_STR(usb_add_function) },
	{ 0x45e30628, __VMLINUX_SYMBOL_STR(usb_put_function) },
	{ 0x52fb9d6b, __VMLINUX_SYMBOL_STR(usb_composite_unregister) },
	{ 0x96164a93, __VMLINUX_SYMBOL_STR(usb_get_function) },
	{ 0xb331827a, __VMLINUX_SYMBOL_STR(usb_string_ids_tab) },
	{ 0x8757b6d1, __VMLINUX_SYMBOL_STR(usb_add_config_only) },
	{ 0x1d65bf12, __VMLINUX_SYMBOL_STR(usb_get_function_instance) },
	{ 0xafad493b, __VMLINUX_SYMBOL_STR(param_ops_ushort) },
	{ 0x47c8baf4, __VMLINUX_SYMBOL_STR(param_ops_uint) },
	{ 0xdd3b32b8, __VMLINUX_SYMBOL_STR(usb_remove_function) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=libcomposite";


MODULE_INFO(srcversion, "FCB68BE6555601C0DFA737D");
