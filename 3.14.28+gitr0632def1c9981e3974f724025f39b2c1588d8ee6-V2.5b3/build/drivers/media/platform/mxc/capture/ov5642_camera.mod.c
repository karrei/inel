#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xf9f72e25, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x18e86401, __VMLINUX_SYMBOL_STR(clk_unprepare) },
	{ 0xd31ccb06, __VMLINUX_SYMBOL_STR(of_machine_is_compatible) },
	{ 0xe4c20a78, __VMLINUX_SYMBOL_STR(i2c_master_send) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0xec270f50, __VMLINUX_SYMBOL_STR(regulator_set_voltage) },
	{ 0x4826b129, __VMLINUX_SYMBOL_STR(clk_enable) },
	{ 0x980f49da, __VMLINUX_SYMBOL_STR(i2c_del_driver) },
	{ 0x244c4fbc, __VMLINUX_SYMBOL_STR(mxc_camera_common_unlock) },
	{ 0x45d82fb7, __VMLINUX_SYMBOL_STR(regulator_disable) },
	{ 0xb7740045, __VMLINUX_SYMBOL_STR(pinctrl_select_state) },
	{ 0xf1ba473d, __VMLINUX_SYMBOL_STR(i2c_transfer) },
	{ 0x750522c7, __VMLINUX_SYMBOL_STR(clk_disable) },
	{ 0x3ebfbb9e, __VMLINUX_SYMBOL_STR(devm_pinctrl_get) },
	{ 0xf816c866, __VMLINUX_SYMBOL_STR(gpio_to_desc) },
	{ 0xdc5e8e97, __VMLINUX_SYMBOL_STR(of_property_read_u32_array) },
	{ 0xa12d929d, __VMLINUX_SYMBOL_STR(desc_to_gpio) },
	{ 0x6c66b424, __VMLINUX_SYMBOL_STR(pinctrl_lookup_state) },
	{ 0xe707d823, __VMLINUX_SYMBOL_STR(__aeabi_uidiv) },
	{ 0xfa2a45e, __VMLINUX_SYMBOL_STR(__memzero) },
	{ 0x5115b907, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x7fd1ec85, __VMLINUX_SYMBOL_STR(v4l2_int_device_register) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x649e4710, __VMLINUX_SYMBOL_STR(of_get_named_gpiod_flags) },
	{ 0x2c3387a6, __VMLINUX_SYMBOL_STR(mxc_camera_common_lock) },
	{ 0x8e865d3c, __VMLINUX_SYMBOL_STR(arm_delay_ops) },
	{ 0x2a202829, __VMLINUX_SYMBOL_STR(v4l2_int_device_unregister) },
	{ 0xe322e1bc, __VMLINUX_SYMBOL_STR(devm_gpio_request_one) },
	{ 0x2196324, __VMLINUX_SYMBOL_STR(__aeabi_idiv) },
	{ 0xcd807001, __VMLINUX_SYMBOL_STR(i2c_register_driver) },
	{ 0xcb9175d0, __VMLINUX_SYMBOL_STR(devm_regulator_get) },
	{ 0xd4b4614c, __VMLINUX_SYMBOL_STR(clk_prepare) },
	{ 0xee6b71c4, __VMLINUX_SYMBOL_STR(syscon_regmap_lookup_by_compatible) },
	{ 0x985146c1, __VMLINUX_SYMBOL_STR(devm_clk_get) },
	{ 0x1d0b9629, __VMLINUX_SYMBOL_STR(i2c_master_recv) },
	{ 0x1e6def96, __VMLINUX_SYMBOL_STR(devm_pinctrl_put) },
	{ 0x687934e9, __VMLINUX_SYMBOL_STR(gpiod_set_raw_value) },
	{ 0x9522c3fe, __VMLINUX_SYMBOL_STR(dev_warn) },
	{ 0x9dedc228, __VMLINUX_SYMBOL_STR(regmap_update_bits) },
	{ 0x17ec9901, __VMLINUX_SYMBOL_STR(regulator_enable) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=mxc_v4l2_capture,v4l2-int-device";

MODULE_ALIAS("i2c:ov5642");
MODULE_ALIAS("i2c:ov564x");

MODULE_INFO(srcversion, "6163ED14CDF70CBE4A79C50");
