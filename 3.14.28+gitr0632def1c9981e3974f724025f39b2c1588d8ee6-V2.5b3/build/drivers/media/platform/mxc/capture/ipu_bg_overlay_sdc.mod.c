#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xf9f72e25, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x92b57248, __VMLINUX_SYMBOL_STR(flush_work) },
	{ 0x2d3385d3, __VMLINUX_SYMBOL_STR(system_wq) },
	{ 0xb75e81c6, __VMLINUX_SYMBOL_STR(ipu_get_soc) },
	{ 0x484fbf21, __VMLINUX_SYMBOL_STR(ipu_check_task) },
	{ 0xc755e3aa, __VMLINUX_SYMBOL_STR(ipu_queue_task) },
	{ 0x909fc0bf, __VMLINUX_SYMBOL_STR(arm_dma_ops) },
	{ 0x4205ad24, __VMLINUX_SYMBOL_STR(cancel_work_sync) },
	{ 0xd69f4ca6, __VMLINUX_SYMBOL_STR(ipu_init_channel) },
	{ 0x8b811a8a, __VMLINUX_SYMBOL_STR(ipu_uninit_channel) },
	{ 0xa783d0c4, __VMLINUX_SYMBOL_STR(ipu_init_channel_buffer) },
	{ 0xfa2a45e, __VMLINUX_SYMBOL_STR(__memzero) },
	{ 0xa085dfa7, __VMLINUX_SYMBOL_STR(ipu_disable_channel) },
	{ 0x74b31fed, __VMLINUX_SYMBOL_STR(ipu_enable_channel) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x935ccf6a, __VMLINUX_SYMBOL_STR(mipi_csi2_get_bind_csi) },
	{ 0x46209a7b, __VMLINUX_SYMBOL_STR(ipu_disable_csi) },
	{ 0xc9890939, __VMLINUX_SYMBOL_STR(ipu_clear_irq) },
	{ 0xc09af8de, __VMLINUX_SYMBOL_STR(ipu_free_irq) },
	{ 0xfb0d7705, __VMLINUX_SYMBOL_STR(mipi_csi2_get_bind_ipu) },
	{ 0x71bde8bc, __VMLINUX_SYMBOL_STR(mipi_csi2_pixelclk_enable) },
	{ 0x3803bbe8, __VMLINUX_SYMBOL_STR(ipu_csi_get_sensor_protocol) },
	{ 0x2702a489, __VMLINUX_SYMBOL_STR(mipi_csi2_get_status) },
	{ 0xe170c470, __VMLINUX_SYMBOL_STR(mipi_csi2_get_info) },
	{ 0x6519f91c, __VMLINUX_SYMBOL_STR(ipu_enable_csi) },
	{ 0x6ac8fdc6, __VMLINUX_SYMBOL_STR(mipi_csi2_pixelclk_disable) },
	{ 0x950ffd7b, __VMLINUX_SYMBOL_STR(ipu_select_buffer) },
	{ 0x8d33365b, __VMLINUX_SYMBOL_STR(mipi_csi2_get_virtual_channel) },
	{ 0xb2d48a2e, __VMLINUX_SYMBOL_STR(queue_work_on) },
	{ 0xd92b8185, __VMLINUX_SYMBOL_STR(ipu_request_irq) },
	{ 0x75dddbb0, __VMLINUX_SYMBOL_STR(mipi_csi2_get_datatype) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "67C2123646AF3EC955C8583");
