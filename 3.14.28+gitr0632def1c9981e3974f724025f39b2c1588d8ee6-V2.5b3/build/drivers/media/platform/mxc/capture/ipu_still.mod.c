#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xf9f72e25, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xd69f4ca6, __VMLINUX_SYMBOL_STR(ipu_init_channel) },
	{ 0x8b811a8a, __VMLINUX_SYMBOL_STR(ipu_uninit_channel) },
	{ 0xa783d0c4, __VMLINUX_SYMBOL_STR(ipu_init_channel_buffer) },
	{ 0xfa2a45e, __VMLINUX_SYMBOL_STR(__memzero) },
	{ 0xa085dfa7, __VMLINUX_SYMBOL_STR(ipu_disable_channel) },
	{ 0x74b31fed, __VMLINUX_SYMBOL_STR(ipu_enable_channel) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x46209a7b, __VMLINUX_SYMBOL_STR(ipu_disable_csi) },
	{ 0xc9890939, __VMLINUX_SYMBOL_STR(ipu_clear_irq) },
	{ 0xc09af8de, __VMLINUX_SYMBOL_STR(ipu_free_irq) },
	{ 0xd85cd67e, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0x6519f91c, __VMLINUX_SYMBOL_STR(ipu_enable_csi) },
	{ 0x950ffd7b, __VMLINUX_SYMBOL_STR(ipu_select_buffer) },
	{ 0xd92b8185, __VMLINUX_SYMBOL_STR(ipu_request_irq) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "E0317E64902898FD30CEF25");
