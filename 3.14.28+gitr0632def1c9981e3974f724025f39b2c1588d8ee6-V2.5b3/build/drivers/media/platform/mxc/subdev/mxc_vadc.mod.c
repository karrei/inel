#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xf9f72e25, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x1a536f9b, __VMLINUX_SYMBOL_STR(platform_driver_unregister) },
	{ 0xaa748863, __VMLINUX_SYMBOL_STR(__platform_driver_register) },
	{ 0xc2165d85, __VMLINUX_SYMBOL_STR(__arm_iounmap) },
	{ 0x5115b907, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x120547e5, __VMLINUX_SYMBOL_STR(of_iomap) },
	{ 0xabc019c6, __VMLINUX_SYMBOL_STR(of_find_compatible_node) },
	{ 0xe8b554e3, __VMLINUX_SYMBOL_STR(syscon_regmap_lookup_by_phandle) },
	{ 0xdc5e8e97, __VMLINUX_SYMBOL_STR(of_property_read_u32_array) },
	{ 0x6b2632bc, __VMLINUX_SYMBOL_STR(__pm_runtime_resume) },
	{ 0x240cb93e, __VMLINUX_SYMBOL_STR(pm_runtime_enable) },
	{ 0x179b9373, __VMLINUX_SYMBOL_STR(v4l2_async_register_subdev) },
	{ 0xb81960ca, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x3cf51020, __VMLINUX_SYMBOL_STR(v4l2_subdev_init) },
	{ 0xa54886e2, __VMLINUX_SYMBOL_STR(dev_set_drvdata) },
	{ 0xd4b4614c, __VMLINUX_SYMBOL_STR(clk_prepare) },
	{ 0x985146c1, __VMLINUX_SYMBOL_STR(devm_clk_get) },
	{ 0xb549551, __VMLINUX_SYMBOL_STR(devm_ioremap_resource) },
	{ 0xc08b9af0, __VMLINUX_SYMBOL_STR(platform_get_resource_byname) },
	{ 0xc824f957, __VMLINUX_SYMBOL_STR(devm_kmalloc) },
	{ 0x4826b129, __VMLINUX_SYMBOL_STR(clk_enable) },
	{ 0x9dedc228, __VMLINUX_SYMBOL_STR(regmap_update_bits) },
	{ 0x18e86401, __VMLINUX_SYMBOL_STR(clk_unprepare) },
	{ 0x5025eef8, __VMLINUX_SYMBOL_STR(v4l2_async_unregister_subdev) },
	{ 0xd84e9974, __VMLINUX_SYMBOL_STR(__pm_runtime_disable) },
	{ 0x5d9516ad, __VMLINUX_SYMBOL_STR(__pm_runtime_idle) },
	{ 0x750522c7, __VMLINUX_SYMBOL_STR(clk_disable) },
	{ 0xe744154e, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("of:N*T*Cfsl,imx6sx-vadc*");

MODULE_INFO(srcversion, "C07043AB8217F8242C71635");
