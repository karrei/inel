#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xf9f72e25, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x20e1628, __VMLINUX_SYMBOL_STR(vb2_ops_wait_finish) },
	{ 0x5c2bd422, __VMLINUX_SYMBOL_STR(vb2_ops_wait_prepare) },
	{ 0x96773cd9, __VMLINUX_SYMBOL_STR(video_ioctl2) },
	{ 0x9394460, __VMLINUX_SYMBOL_STR(vb2_fop_poll) },
	{ 0x1a536f9b, __VMLINUX_SYMBOL_STR(platform_driver_unregister) },
	{ 0xaa748863, __VMLINUX_SYMBOL_STR(__platform_driver_register) },
	{ 0x4617ca43, __VMLINUX_SYMBOL_STR(__pm_runtime_suspend) },
	{ 0x60199952, __VMLINUX_SYMBOL_STR(release_bus_freq) },
	{ 0x92d86bea, __VMLINUX_SYMBOL_STR(vb2_queue_release) },
	{ 0x750522c7, __VMLINUX_SYMBOL_STR(clk_disable) },
	{ 0xe57f0426, __VMLINUX_SYMBOL_STR(vb2_dma_contig_cleanup_ctx) },
	{ 0x50461308, __VMLINUX_SYMBOL_STR(request_bus_freq) },
	{ 0x6b2632bc, __VMLINUX_SYMBOL_STR(__pm_runtime_resume) },
	{ 0x4a864cbd, __VMLINUX_SYMBOL_STR(vb2_queue_init) },
	{ 0x8dd375bb, __VMLINUX_SYMBOL_STR(vb2_dma_contig_memops) },
	{ 0xd1b34ead, __VMLINUX_SYMBOL_STR(vb2_dma_contig_init_ctx) },
	{ 0x18e86401, __VMLINUX_SYMBOL_STR(clk_unprepare) },
	{ 0x4826b129, __VMLINUX_SYMBOL_STR(clk_enable) },
	{ 0xd4b4614c, __VMLINUX_SYMBOL_STR(clk_prepare) },
	{ 0x9c0bd51f, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0x9522c3fe, __VMLINUX_SYMBOL_STR(dev_warn) },
	{ 0x264fb321, __VMLINUX_SYMBOL_STR(vb2_buffer_done) },
	{ 0x1c483a9, __VMLINUX_SYMBOL_STR(v4l2_get_timestamp) },
	{ 0x909fc0bf, __VMLINUX_SYMBOL_STR(arm_dma_ops) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0xf216000a, __VMLINUX_SYMBOL_STR(v4l2_of_get_remote_port_parent) },
	{ 0xc07e835b, __VMLINUX_SYMBOL_STR(of_get_next_child) },
	{ 0x240cb93e, __VMLINUX_SYMBOL_STR(pm_runtime_enable) },
	{ 0x36e2fdae, __VMLINUX_SYMBOL_STR(v4l2_async_notifier_register) },
	{ 0xaafdc258, __VMLINUX_SYMBOL_STR(strcasecmp) },
	{ 0x7c08fc59, __VMLINUX_SYMBOL_STR(of_get_next_available_child) },
	{ 0x5115b907, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0xda5cb50e, __VMLINUX_SYMBOL_STR(devm_request_threaded_irq) },
	{ 0x7e87b25, __VMLINUX_SYMBOL_STR(__video_register_device) },
	{ 0xa54886e2, __VMLINUX_SYMBOL_STR(dev_set_drvdata) },
	{ 0x6751a326, __VMLINUX_SYMBOL_STR(video_device_release) },
	{ 0x29639abd, __VMLINUX_SYMBOL_STR(video_device_alloc) },
	{ 0x94a28e7d, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x9b8bce3a, __VMLINUX_SYMBOL_STR(v4l2_device_register) },
	{ 0x985146c1, __VMLINUX_SYMBOL_STR(devm_clk_get) },
	{ 0xb549551, __VMLINUX_SYMBOL_STR(devm_ioremap_resource) },
	{ 0x10be6366, __VMLINUX_SYMBOL_STR(platform_get_irq) },
	{ 0x7965b685, __VMLINUX_SYMBOL_STR(platform_get_resource) },
	{ 0xc824f957, __VMLINUX_SYMBOL_STR(devm_kmalloc) },
	{ 0xa4653043, __VMLINUX_SYMBOL_STR(vb2_read) },
	{ 0x5448e0bc, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x4836f3ec, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x68f292cc, __VMLINUX_SYMBOL_STR(vb2_mmap) },
	{ 0xe56d0f4c, __VMLINUX_SYMBOL_STR(mutex_lock_interruptible) },
	{ 0x50bd3bb3, __VMLINUX_SYMBOL_STR(vb2_plane_vaddr) },
	{ 0x51d559d1, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x598542b2, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0xb81960ca, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x73e20c1c, __VMLINUX_SYMBOL_STR(strlcpy) },
	{ 0x79a37e3d, __VMLINUX_SYMBOL_STR(vb2_reqbufs) },
	{ 0x5a8c4cdb, __VMLINUX_SYMBOL_STR(vb2_plane_cookie) },
	{ 0xd10c889e, __VMLINUX_SYMBOL_STR(vb2_querybuf) },
	{ 0x6d45d536, __VMLINUX_SYMBOL_STR(vb2_qbuf) },
	{ 0xf17ea67c, __VMLINUX_SYMBOL_STR(vb2_dqbuf) },
	{ 0xd0e448d4, __VMLINUX_SYMBOL_STR(vb2_streamon) },
	{ 0xa19b3c93, __VMLINUX_SYMBOL_STR(vb2_streamoff) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xc8aa841d, __VMLINUX_SYMBOL_STR(video_devdata) },
	{ 0xd84e9974, __VMLINUX_SYMBOL_STR(__pm_runtime_disable) },
	{ 0x16ecd29, __VMLINUX_SYMBOL_STR(v4l2_device_unregister) },
	{ 0xb7905b15, __VMLINUX_SYMBOL_STR(video_unregister_device) },
	{ 0xed2e41c1, __VMLINUX_SYMBOL_STR(v4l2_async_notifier_unregister) },
	{ 0xe744154e, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xe707d823, __VMLINUX_SYMBOL_STR(__aeabi_uidiv) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=videobuf2-dma-contig";

MODULE_ALIAS("of:N*T*Cfsl,imx6s-csi*");

MODULE_INFO(srcversion, "2624BC35456EAEB7DF67313");
