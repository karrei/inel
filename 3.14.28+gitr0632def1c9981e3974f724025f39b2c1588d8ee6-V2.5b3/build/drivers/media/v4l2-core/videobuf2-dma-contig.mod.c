#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xf9f72e25, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x2b964b94, __VMLINUX_SYMBOL_STR(vb2_put_vma) },
	{ 0xa31d5a02, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x12da5bb2, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x5c6925fa, __VMLINUX_SYMBOL_STR(mem_map) },
	{ 0x9e453a38, __VMLINUX_SYMBOL_STR(page_address) },
	{ 0x909fc0bf, __VMLINUX_SYMBOL_STR(arm_dma_ops) },
	{ 0x830ca2f6, __VMLINUX_SYMBOL_STR(dma_buf_detach) },
	{ 0xc42b57cf, __VMLINUX_SYMBOL_STR(set_page_dirty_lock) },
	{ 0xd5152710, __VMLINUX_SYMBOL_STR(sg_next) },
	{ 0x4836f3ec, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x51e77c97, __VMLINUX_SYMBOL_STR(pfn_valid) },
	{ 0x20b6e8d2, __VMLINUX_SYMBOL_STR(__pv_phys_offset) },
	{ 0x4a615f20, __VMLINUX_SYMBOL_STR(dma_common_get_sgtable) },
	{ 0x6b117feb, __VMLINUX_SYMBOL_STR(follow_pfn) },
	{ 0x5115b907, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0x5448e0bc, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0xf4a4a97, __VMLINUX_SYMBOL_STR(vb2_get_vma) },
	{ 0xb6e28564, __VMLINUX_SYMBOL_STR(dma_buf_unmap_attachment) },
	{ 0xcdd158d, __VMLINUX_SYMBOL_STR(sg_alloc_table) },
	{ 0x2d118a34, __VMLINUX_SYMBOL_STR(find_vma) },
	{ 0x9346bbc4, __VMLINUX_SYMBOL_STR(kmem_cache_alloc) },
	{ 0x4999e94, __VMLINUX_SYMBOL_STR(dma_buf_map_attachment) },
	{ 0x2c78d3fb, __VMLINUX_SYMBOL_STR(put_device) },
	{ 0xce09eff8, __VMLINUX_SYMBOL_STR(get_user_pages) },
	{ 0x747115d2, __VMLINUX_SYMBOL_STR(dma_buf_attach) },
	{ 0x18d8f7e4, __VMLINUX_SYMBOL_STR(get_device) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xe442be52, __VMLINUX_SYMBOL_STR(vb2_common_vm_ops) },
	{ 0xd1a07850, __VMLINUX_SYMBOL_STR(put_page) },
	{ 0x9712d709, __VMLINUX_SYMBOL_STR(sg_alloc_table_from_pages) },
	{ 0xabcb59f8, __VMLINUX_SYMBOL_STR(dma_buf_export_named) },
	{ 0xa777f79b, __VMLINUX_SYMBOL_STR(dma_common_mmap) },
	{ 0x9cd60539, __VMLINUX_SYMBOL_STR(sg_free_table) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=videobuf2-memops";


MODULE_INFO(srcversion, "77088760F15CA672AC78D2D");
